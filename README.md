---
lang: fr
---

# TD8 - Objets et classes : course de voitures

## Course simple

Nous allons d’abord écrire un programme qui organise une course entre deux voitures, en ligne
droite d’un point de départ à un point d’arrivée.

### La classe Voiture

Une voiture est caractérisée par son **nom** (chaîne de caractères), sa **position** sur un axe à partir d’un point origine (entier positif ou nul) et sa **vitesse** (entier positif ou nul). Pour la simulation à l’écran, l’axe sera horizontal et le point origine tout à fait à gauche de l’écran.

La classe `Voiture` a donc **trois attributs** : nom, position et vitesse.

Consultez le squelette de code de la classe `Voiture` et complétez le code. Il faudra définir les **attributs** et complétez les différentes **méthodes**.

**Conseil** : La méthode `charAt(int i)` invoquée par un objet de la classe `String` renvoie le ième caractère de la chaîne (en commençant à l'indice 0, comme pour un tableau).

### La classe Course

Une **course** (entre 2 **voitures**) est caractérisée par les 2 voitures en compétition, qui sont des instances de la classe Voiture, ainsi que par la longueur de la piste, qui est un entier strictement positif.

Le déroulement d’une course se passe de la façon suivante :

* Au début, les 2 voitures sont placées sur la ligne de départ. 

* A chaque étape, l’une des 2 voitures, choisie aléatoirement, avance. 

* La course s’arrête dès qu’une voiture a franchi la ligne d’arrivée.

La classe Course a donc 3 **attributs** : **voit1**, **voit2** et **longueurPiste**.

Consultez le squelette de code de la classe `Course` et complétez le code. Il faudra définir les **attributs** et complétez les différentes **méthodes**.

**Conseils** :

* La méthode de classe `Ut.randomMinMax(int min, int max)` permet de renvoyer un nombre entier pseudo-aléatoire compris entre min et max.
* Dans la console, pour simuler l’évolution des voitures, vous pouvez utiliser les procédures `Ut.clearConsole()` et `Ut.pause(int timeMilli)` entre deux étapes de la course.


### La classe MainCourse

La classe `MainCourse` permet d’exécuter des courses entre des voitures. 

La procédure principale (`main`) doit créer deux instances **v1** et **v2** de la classe `Voiture` et une instance de la classe `Course` mettant en compétition **v1** et **v2**. La procédure déclenche ensuite le déroulement de la **course** entre **v1** et **v2** et affiche finalement **le nom de la voiture gagnante**.

Tester votre programme en implémentant ce comportement.

## Course sur plusieurs longueurs de piste

On considère maintenant une **course de voitures** qui se déroule non pas sur une, mais sur **k** longueurs de piste, où **k** est un entier **strictement positif**, en effectuant des **aller-retours**. Ainsi, la version de base est le cas particulier où **k** est égal à 1, et pour k égal à 2, le trajet est un aller-retour de la piste. Le **point d’arrivée** est l’origine si k est **pair** et le bout de la piste sinon.

On suppose que quand une voiture arrive à l’une des 2 extrémités de la piste, elle « rebondit » et termine son déplacement dans l’autre sens pour parcourir en tout une distance égale à sa **vitesse**. Par exemple, si la voiture a une **vitesse** de 10, est à la **position** 7 et se dirige vers l’origine, elle rebondit et s’arrête à la position 3 puisque 7 + 3 = 10.

Ajouter à la classe `Course` un attribut **k** indiquant le **nombre de longueurs de piste à parcourir**.

Modifier la classe `Voiture` pour les besoins de cette course plus générale. Pour cela, vous pouvez suivre les étapes suivantes :

### Première étape

Vous devez définir et utiliser les attributs suivants :

* Un attribut entier **sens** (dans `Voiture`) qui représente le sens de déplacement sur l’axe : il vaut 1 si la voiture s’éloigne de l’origine et -1 si elle s’en rapproche.
* Un attribut **nbLongPiste** (dans `Course`) qui contient le nombre de longueurs de piste déjà parcourues.

### Deuxième étape

Vous devez définir et utiliser un attribut entier (dans `Voiture`) qui permet de connaitre la distance parcourue par la voiture depuis le début de la course.

### Troisième étape

Vous devez définir et utiliser les **attributs de votre choix**.

Dans `Voiture`, la méthode `avance` doit être modifiée pour tenir compte des **rebondissements**, et vous pouvez supprimer, modifier ou ajouter d’autres méthodes.

**Pour les étapes 1 et 3** : on suppose que la vitesse d’une voiture est toujours inférieure à la longueur de la piste, de sorte qu’une voiture rebondit au plus une fois lors d’un déplacement.

## Extensions possibles

Voici différentes extensions possibles que vous pouvez implémenter afin d'améliorer votre programme :

1. Donner aux 2 **voitures** des **vitesses différentes** et des **probabilités différentes** d’être choisies pour avancer (en équilibrant les chances pour une course équitable !).

2. **Défavoriser une voiture au départ**, mais pour équilibrer les chances, permettre à une voiture d’accélérer quand son écart avec la voiture de tête devient trop important. Pour cela, il vous faudra rajouter des méthodes à la classe `Voiture` pour permettre à une voiture d’accélérer et pour pouvoir évaluer l’écart entre 2 voitures.

3. Simuler une **course** entre **un nombre quelconque de voitures** dans `MainCourse` ou une nouvelle classe dédiée. Pour cela, utilisez un **tableau** d’instances de la classe `Voiture`.

4. Faire évoluer la voiture dans un **plan** : remplacer l’attribut **position** par les coordonnées **x** et **y**, et l’attribut **sens** par une **direction** pouvant avoir 4 valeurs possibles (nord, sud, ouest, est). Pour l’affichage, on peut utiliser une **matrice de caractères** (tableau de tableaux de caractères) ou bien donner accès aux coordonnées des **voitures** en ajoutant des accesseurs `getLigne()` et `getColonne()` (ou `getY()` et `getX()`) dans la classe `Voiture`.